require_relative "empty_input_error.rb"
require_relative "capitalization_error.rb"

class Name
  NOT_CAPITAL_ALPHABET = /^[A-Z]/
  def initialize(firstname, lastname)
    raise EmptyInputError, "Empty Firstname", caller if is_empty?(firstname)
    raise EmptyInputError, "Empty Lastname", caller if is_empty?(lastname)
    raise CapitalizationError, "Firstname is not Capitalize", caller unless is_capitalize?(firstname)
    @firstname = firstname
    @lastname = lastname
  end

  protected
  def is_capitalize?(string)
    (string =~ NOT_CAPITAL_ALPHABET) == 0
  end

  def is_empty?(string)
    string.to_s.strip.empty?
  end
end

