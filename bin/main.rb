require_relative "../lib/name.rb"

begin
p Name.new("Pankaj", "Kumar")
p Name.new("", "Kumar")
rescue EmptyInputError, CapitalizationError => detail
  p detail
end

